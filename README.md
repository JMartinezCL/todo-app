# TodoApp

Todoapp is an application to remember things that you want to do. It's was develped on flutter with Sqflite for a small database.

Features on the TodoApp include:
- Create todo; name, description and priority.
- Modify the todos that was added before.
- Delete the todo that you want.

## Getting Started
- Download zip project or clone git repository on your local workspace.
- Connect mobile device to your computer, or start emulated device.
- Run "flutter doctor -v" to check everything is ok.
- Run "flutter run" or "flutter run -v" for more info.

## Screenshoots

<p align="center">
  <img src="images/sc01.png" width="200" alt="1"/>
  <img src="images/sc02.png" width="200" alt="2"/> 
  <img src="images/sc03.png" width="200" alt="3"/>
  <img src="images/sc04.png" width="200" alt="4"/>
</p>

<p align="center">
  <img src="images/sc05.png" width="200" alt="5"/>
  <img src="images/sc06.png" width="200" alt="6"/> 
  <img src="images/sc07.png" width="200" alt="7"/>
</p>

**-Screenshoot get on Android One Pie, API level 29.**

# Authors
* **José Martínez** - [JMartinezCL](https://gitlab.com/JMartinezCL)